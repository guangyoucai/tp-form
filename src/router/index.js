import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/index.vue'
import Show from '../views/show.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/show',
    component: Show
  }
]

const router = new VueRouter({
  routes
})

export default router
